export class Task
{

	constructor(	
		public id : number,
		public name : string,
		public status: boolean
	) {}

	getName() {
		return this.name.toUpperCase();
	}

	// Si status = false afficher "to do"
	// Si status = true afficher "done"
	getStatus() {
		return this.status ? "done" : "to do";
	}

}