import { Component, OnInit } from '@angular/core';
import { Task } from '../models/task';
import { TaskService } from '../services/task.service';

@Component({
	selector: 'app-list',
	templateUrl: './list.component.html',
	styleUrls: ['./list.component.sass'],
	providers: [ TaskService ]
})
export class ListComponent implements OnInit {
	
	tasks: Task[] = [];
	defaults: Task[];
	keywords : string = '';

	constructor(
		private taskService: TaskService
	) {
		this.defaults = this.tasks;
		this.tasks = this.taskService.getAll();
	}

	ngOnInit() {
	}

	filter() {
		this.tasks = this.defaults;

		this.tasks = this.tasks.filter(task => 
			task.getName().indexOf(
				this.keywords.toUpperCase()
			) != -1
		);
	}

	change(task) {
		task.status = !task.status;
		this.taskService.setToStorage();
	}

	remove(task) {
		this.taskService.remove(task);
	}

}
