import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
	selector: 'app-create',
	templateUrl: './create.component.html',
	styleUrls: ['./create.component.sass'],
})
export class CreateComponent implements OnInit {

	@Input() service;

	constructor(
		private fb: FormBuilder,
	) { }

	success : boolean = false;

	taskForm = this.fb.group({
		name: ['', Validators.compose([
			Validators.required,
			Validators.minLength(3),
			Validators.maxLength(15),
		])],
		status: ['', Validators.required]
	});

	submit() : void {
		this.service.add(
			this.taskForm.value
		);

		this.taskForm.reset();
		this.success = true;
	}

	ngOnInit() {
	}

}
